package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void setFlyBehavior(FlyBehavior newFlyBehave) {
        flyBehavior = newFlyBehave;
    }

    public void setQuackBehavior(QuackBehavior newQuackBehave) {
        quackBehavior = newQuackBehave;
    }

    public abstract void display();

    public void swim() {
        System.out.println("All ducks float, even decoys!");
    }
    // TODO Complete me!
}
